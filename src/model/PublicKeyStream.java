/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.security.PublicKey;

/**
 *
 * @author Matheus
 */
public class PublicKeyStream implements Serializable {

    private String motivo;
    private PublicKey chavePublica;
    private String caminhoChave;
    private String nomeArquivo;

    public PublicKeyStream() {
    }

    public PublicKeyStream(String motivo) {
        this.motivo = motivo;
    }

    public PublicKeyStream(String motivo, String nomeArquivo) {
        this.motivo = motivo;
        this.nomeArquivo = nomeArquivo;
    }

    public PublicKeyStream(String nomeArquivo, PublicKey chavePublica) {
        this.nomeArquivo = nomeArquivo;
        this.chavePublica = chavePublica;
    }

    public PublicKeyStream(String motivo, PublicKey chavePublica, String caminhoChave, String nomeArquivo) {
        this.motivo = motivo;
        this.chavePublica = chavePublica;
        this.caminhoChave = caminhoChave;
        this.nomeArquivo = nomeArquivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public PublicKey getChavePublica() {
        return chavePublica;
    }

    public void setChavePublica(PublicKey chavePublica) {
        this.chavePublica = chavePublica;
    }

    public String getCaminhoChave() {
        return caminhoChave;
    }

    public void setCaminhoChave(String caminhoChave) {
        this.caminhoChave = caminhoChave;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

}
