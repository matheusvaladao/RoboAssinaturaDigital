/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;
import javax.naming.NamingException;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author Edward Neto
 */
public class SisUtil {
    
    private static Properties getProp() throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream("C:\\medical\\medicalbd\\smstransfer.properties");
        props.load(file);
        return props;
    }
    
    /**
     * Método que cria o corpo do e-mail de Erro de Conexão e o Envia.
     * Envia e-mail para destinatário contendo o erro, o ip e a porta de conexão que foram realizadas as 
     * tentativas.
     * @param ex
     * @param ip
     * @param porta
     * @throws GeneralSecurityException
     * @throws NamingException
     * @throws EmailException
     * @throws IOException 
     */
    public static void enviaEmailErro(IOException ex, String ip, int porta, String dest) throws GeneralSecurityException, NamingException, EmailException, IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AplicacaoRoboSocket: Ocorreu um erro na tentativa de conectar ao Servidor de Arquivos.");
        stringBuilder.append("\n IP: ").append(ip).append(" Porta: ").append(porta);
        stringBuilder.append("\n Exceção: ").append(ex);
        stringBuilder.append("\n Data: ").append(new Date());
        stringBuilder.append("\n Identificado por: AplicacaoRoboSocket.");
        SisUtil.enviaEmail("Servidor de Arquivos: ERRO", stringBuilder.toString(), dest);
    }
    
    /**
     * Envia o e-mail de acordo com os dados passados por parametro. Caso haja
     * algum erro, sera retornado o valor boolean 'false'. E-mail remetente em
     * 'Codigos.EMAIL_FROM'.
     *
     * @param assunto
     * @param mensagem
     * @param destinatario
     * @return
     * @throws java.security.GeneralSecurityException
     * @throws javax.naming.NamingException
     * @throws org.apache.commons.mail.EmailException
     * @throws java.io.IOException
     */
    public static boolean enviaEmail(String assunto, String mensagem, String destinatario) throws GeneralSecurityException, NamingException, EmailException, IOException {
        long time = System.currentTimeMillis();

        Properties prop;
        prop = SisUtil.getProp();
        String emailContatoSite = prop.getProperty("EMAIL");
        String senhaEmail = prop.getProperty("SENHAEMAIL");
        String smtp = prop.getProperty("SMTP");
        String portaSmtp = prop.getProperty("PORTASMTP");
        
        //Descobrindo a quantidade de cópias de e-mails
        int quantidadeCc = Integer.parseInt(prop.getProperty("NUMERO_CC"));
        String[] emailCc = new String[quantidadeCc];
        int i = 0;
        while (i < quantidadeCc) {
            emailCc[i] = prop.getProperty(("CC_" + i));
            System.out.println(("CC_" + i));
            i++;
        }
        //Fim
        
        System.out.println("Email: " + emailContatoSite);
        System.out.println("Senha: " + senhaEmail);

        try {
            SimpleEmail email = new SimpleEmail();
            email.setHostName(smtp);
            email.setStartTLSEnabled(true);
            email.setSmtpPort(Integer.parseInt(portaSmtp));
            email.setAuthenticator(new DefaultAuthenticator(emailContatoSite, senhaEmail));
            email.setFrom(emailContatoSite);
            email.setDebug(true);
            email.setSubject(assunto);
            email.setMsg(mensagem);
            email.addTo(destinatario);
            if (emailCc != null) {
                for (String emailCc1 : emailCc) {
                    email.addCc(emailCc1);
                }
            }
            email.setAuthentication(emailContatoSite, senhaEmail);

            email.send();
            System.out.println("  SisUtil.enviaEmail: E-mail enviado: " + (System.currentTimeMillis() - time) + " ms.");
            return true;
        } catch (EmailException ex) {
            System.out.println("  SisUtil.enviaEmail: ERRO: MessagingException: " + ex.getMessage());
            return false;
        }
    }
    
}
