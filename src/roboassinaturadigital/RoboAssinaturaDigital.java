/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roboassinaturadigital;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import model.PublicKeyStream;
import org.apache.commons.mail.EmailException;
import sisUtil.SisUtil;

/**
 *
 * @author Matheus
 */
public class RoboAssinaturaDigital implements Runnable {

    private final Socket cliente;
    private static Properties prop;
    private static String IPSC;             //IP do servidor de CHAVES
    private static int PCSC;                //Porta de comunicação do servidor de CHAVES
    private static String DESTINATARIO;
    private static int INTEVALO_EXEC;

    public RoboAssinaturaDigital(Socket cliente) {
        this.cliente = cliente;
    }

    /**
     * Carrega o arquivo de Propriedades.
     *
     * @return
     * @throws IOException
     */
    private static Properties getProp() throws IOException {
        Properties props = new Properties();
        InputStream is = RoboAssinaturaDigital.class.getClass().getResourceAsStream("/propriedades/smstransfer.properties");

        if (is == null) {
            new Exception("Arquivo não encontrado.");
        }
        props.load(is);
        return props;
    }

    /**
     * Seta as variáveis do arquivo de propriedade. Método criado para setar as
     * variáveis na classe ClienteTransfer pois a classe possui métodos
     * estáticos.
     */
    private static void carregaPropriedades() {
        try {
            prop = getProp();
            IPSC = prop.getProperty("SERVER_NAME");
            PCSC = Integer.parseInt(prop.getProperty("PORT_DOWNLOAD"));
            DESTINATARIO = prop.getProperty("DESTINATARIO");
            INTEVALO_EXEC = Integer.parseInt(prop.getProperty("INTERVALO_EXEC"));

        } catch (IOException ex) {
            System.out.println("RoboAssinaturaDigital: Erro ao carregar arquivo de propriedades: " + ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Carrega os parâmetros do arquivo das propriedades
        carregaPropriedades();
        System.out.println(IPSC + " - " + PCSC + " - " + DESTINATARIO + " - " + INTEVALO_EXEC);

        final long tempo = (((INTEVALO_EXEC)) * 5); //Executa a cada 1 hora
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                try {
                    //Cria o socket de comunicação entre o Cliente e o Servidor
                    Socket socket = new Socket(IPSC, PCSC);
                    System.out.println(IPSC + "@" + PCSC);
                    RoboAssinaturaDigital robo = new RoboAssinaturaDigital(socket);
                    Thread t = new Thread(robo);
                    t.start();
                } catch (IOException ex) {
                    System.err.println("AplicacaoRoboSocket: Erro na tentativa de conectar ao Servidor!"
                            + "Erro: " + ex);
                    ex.printStackTrace();
                    try {
                        SisUtil.enviaEmailErro(ex, IPSC, PCSC, DESTINATARIO);
                    } catch (GeneralSecurityException | NamingException | EmailException | IOException ex1) {
                        Logger.getLogger(RoboAssinaturaDigital.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, tempo, tempo);
    }

    @Override
    public void run() {
        ObjectOutputStream outToServer;
        try {
            //Cria o objeto
            outToServer = new ObjectOutputStream(cliente.getOutputStream());
            //Envia um objeto ao servidor com o motivo = ping
            PublicKeyStream pkStream = new PublicKeyStream("ping");
            //Envia ao servidor o documento a ser recuperado
            outToServer.writeObject(pkStream);
            System.out.println("----------"
                    + "\n Tentativa de Conexão com o Servidor"
                    + "\n IP/porta: " + cliente.getInetAddress() + ":" + cliente.getPort()
                    + "\n Hora: " + (new Date())
                    + "\n----------"
                    + "\n Tentativa realizada com Sucesso!");
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

}
